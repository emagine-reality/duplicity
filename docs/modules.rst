duplicity-src8
==============

.. toctree::
   :maxdepth: 4

   compilec
   duplicity
   setup
   testing
